### Audion

Audion is a system for exploring generative music using a 2D environment of computer agents ("birds") and several objects ("observers") a user can place in the environment. 

Each observer can have a sound file associated with it (although this is not completely necessary), and, depending on the role of observer, certain activity by the birds in its proximity will trigger audio playback. 

Each bird has a set of internal motivators: fear, hunger and desire to mate. Additionally, each bird also has a measure of how heavily each motivator is weighted for its individual actions. One bird might respond to fear very strongly and hunger very weakly. The level of the motivators and their corresponding weights controls how an individual bird modifies the audio playback it triggers. 

#### Installation

Audion was written using the LÖVE framework for Lua, and the audio is generated from a patch in Pure Data. 

##### Ubuntu 18.04/19.10

1. Install [Pure Data](www.puredata.info): 

```
$ sudo apt-get install puredata
```

2. It will also require three pd externals: moocow, mrpeach and cyclone. The easiest way to get these is by opening pd and navigating to Help -> Find Externals -> Search & Install. 

3. Next, install [LÖVE](www.love2d.org). 

```
$ sudo add-apt-repository ppa:bartbes/love-stable
$ sudo apt-get update
$ sudo apt install love
$ love --version
LOVE 11.3 (Mysterious Mysteries)
```

4. Clone this repo. 

```
git clone https://gitlab.com/jogawebb/eco-synth.git
```

5. Run the project. 

```
cd eco-synth
pd ./audion/pd/main.pd && ./audion/audion.love
```

#### Using Audion

Once things are running, you should see a window that looks something like this. 

<img title="" src="audion/readme-images/screenshot1.png" alt="" width="500" height="500">

On the left, there is a toolbar. The top four buttons correspond to our four different types of obserers. From top to bottom, we have the **watcher, feeder, hunter**

and **plague**. Below those buttons are two that control system parameters. The Birds menu allows the user to adjust how many birds the simulation supports. The Master menu allows the user to switch the audio on and off and control volume. 

On the right, we see our birds. You see they have bodies (the inner shape), different colors and a range of vision (the outer shape). A triangle or square body indicates the bird's gender. The different colors represent the dominant internal state: red is fear, blue is hunger, and green is desire to mate. The outer shape also indicates this: pentagons mean fear, octogons mean desire to mate, and circles mean hunger. 

Birds have a natural lifespan, but they will also die if they get too hungry. So, to feed, them, we can place a **feeder**. 

<img title="" src="audion/readme-images/screenshot2.png" alt="" width="500" height="500">

Feeders will periodically drop seeds that the birds will eat. When a bird eats a piece of food in the visual range of the feeder (indicated by the white lines), it will trigger the feeder's sound (if loaded). If we right click on the feeder, we can see its properties and adjust them. 

<img title="" src="audion/readme-images/screenshot3.png" alt="" width="500" height="500">

Here, we can control how far the feeder will throw seeds (which also serves as how far away it can "see"). We can choose how loud the sounds it produces are, and load an audio sample. We can also control how many, and in what direction it will drop its seeds. 

<img title="" src="audion/readme-images/watcher-black.png" alt="" width="120" height="120">

Additionally, we can place the **watcher**. A watcher rapidly increases the desire to mate for the birds in its effective radius, even if the watcher is outside the range of vision for the birds. And when a pair of birds mate in its radius, a loaded audio sample will be triggered. 

<img title="" src="audion/readme-images/hunter-black.png" alt="" width="120" height="120">

Similarly, there is the **hunter**, which rapidly increases the birds' fear. When a bird flees, and changes direction to avoid the hunter, the hunter's audio sample is triggered. 

<img title="" src="audion/readme-images/plague-black.png" alt="" width="120" height="120">

Finally, there is the **plague**. The plague does not trigger audio directly. Rather, it will kill off a certain percentage of birds that wander into its effective radius (~15%). But the longer a bird stays near it, the higher that bird's chance of dying :(

#### Loading Audio

There are a number of pre-loaded audio samples to choose from. You can also drag-and-drop files from your computer into Audion. You can also place audio files in the /assets/sounds/ folder, and on the next start, EcAudionill automatically load those files. Currently, only .wav is supported. 

 When you look at an Observer's properties, you will see a drop-down menu with the loaded audio samples. You can select one and click "Upload Sample" to use it as that Observer's audio. You can also use the "Audio Start Position" slider to adjust where in the sample it will begin playing when triggered. 

<img title="" src="audion/readme-images/screenshot4.png" alt="" width="500" height="500">
