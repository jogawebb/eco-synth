## Acknowledgments

I would like to presonally thank Edwin van der Heide and Rob Saunders for their guidance; Zach Barr and Jae Perris for offering valuable feedback and volunteering their time as critics; and my partner Kees, and my family and friends for their loving support during this project. 

[References  >>](references.md)
