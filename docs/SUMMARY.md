# Summary

* [Abstract](README.md)
* [I. An Overture, of Sorts](partOne.md)
* [II. Background](partTwo.md)
* [III. An Ecosystem](partThree.md)
* [IV. Designing an Expressive System](partFour.md)
* [V. Evaluation](partFive.md)
* [VI. Conclusion](conclusions.md)
* [Acknowledgments](acknowledgments.md)
* [References](references.md)
* [Audion on Gitlab](https://gitlab.com/jogawebb/eco-synth)
