## IV. Designing an Expressive System

The design process for Audion began by considering Eno’s fascination with Conway’s Game of Life, and translating that simulation directly into a musical interface. This initial step was envisioned as an initial exploration, to help prioritize elements of control and implementation, rather than as an end unto itself. 

The program's design was inspired by the Tenori-On—a digital musical instrument created by Toshio Iwai for Yamaha—and other two-dimensional step sequencers common in live, electronic music performance. <a href="references.md#nagle">[33]</a> This particular metaphor was chosen for the visual similarities between the Tenori-On interface and graphical representations of Life. 

|    <img src="./media/photos/tenori-on.JPG" height="360">     |
| :----------------------------------------------------------: |
| **Fig. 2**: The Tenori-On, an electronic instrument created by Toshio Iwai. <a href="references.md#nagle">[33, 34]</a> |

The interfaces for this program and the proceeding iterations were realized in LÖVE, a 2D game engine for the Lua programming language. LÖVE was chosen for its perceived speed, simplicity, portability and active community of developers. <a href="references.md#love">[35]</a> Additionally, the Pure Data visual programming language was used to handle audio manipulation and synthesis. <a href="puredata">[36]</a>

In this program, the user can begin by setting initial parameters, such as the width and height of the grid in cells. The grid begins with all cells dead, and by clicking on individual cells, the user can activate them either prior to  starting the simulation (to initialize the simulation with a desired pattern) or as the simulation runs (to interact with or disrupt patterns that form). When the simulation is started, the scan line moves from the top of the grid to the bottom at each time step. The simulation’s cells also advance once each time step. Users can adjust the frequency of the time step in beats per minute. 

| <iframe width="640" height="480" src="https://www.youtube.com/embed/OiHZnwjRnIQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> |
|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| **Video 1**: An implementation of Conway's Game of Life for generating music. Each cell in a horizontal row is mapped to the notes of a C major scale. Notes are played as the scan line moves over a row. |

Each horizontal row represents a time step, and each vertical row represents a note of the C major scale. During each time step, the living cells’ corresponding notes are played. By “drawing” several configurations of cells that move in the plane but oscillate in their form, we hear patterns of note intervals and rhythms emerge from the simulation. 

As previously stated, this implementation was intended to function as a starting point for exploration. While this version meets some of our stated criteria—its mechanics are quite simple, and its cells will carry on or die without user input—we might struggle to suggest it is unpredictable or that its agents' movements represent multiple levels of meaning.

In an attempt to increase the level of interaction and unpredictable behavior in the system, the ability to entirely eliminate any cell from the simulation was added. When a cell is eliminated, it functions as a dead cell that cannot become alive. This adds the ability to control which notes are not played, but doing so has the consequence of forcing the cells' patterns to adjust around the gaps in the simulation. 

| <iframe width="640" height="480" src="https://www.youtube.com/embed/i3bwL_GtlP4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> |
|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| **Video 2**: An iteration of a Game of Life simulation included the ability to direct the movement of the cells by permanently deactivating cells in the grid.                                             |

We are interested in the emergent properties of this (or any) system, but we can only affect those properties by changing the conditions of the simulation’s current time step. In order to produce a desired structure-wide result, we have to predict how our immediate modifications will affect distant time steps. It is this relationship between between desire and outcome that might create the interest for both the listener and the user, and it is these types of changes that we attempted to implement in later iterations of the program. 

#### Using Agent Motivations to Guide Action

As previously discussed, multi-agent systems that model properties such as age, adaptability or desire for shelter of their constituent individuals have advantages over models that are predicated on assumptions such as birth and death rates. <a href="references.md#gras">[31]</a> While this might lead to a “more detailed” simulation, that is not meant to suggest that these models will lead to more inherently interesting output than a similarly sonified, but less complex, simulation. Indeed, Manuel Rocha-Iturbide warned that focusing on the complexity and accuracy of a simulation for sound design runs the risk of rendering the simulation “more important than ourselves, and we become passive, content to merely observe the results.” <a href="references.md#serquera">[qtd., 8] </a> 

As a test of this premise, a simulation with two types of agents was created. Within a two-dimensional space, a population of “birds” and “trees” was initiated. Periodically, each tree produced a random pattern of offspring within a certain radius, with the number of offspring trees  selected at random within a normal distribution. The birds would  forage, searching for trees to eat. Each bird had a randomly generated field of vision, and if a bird was hungry and a tree was within its field of vision, it would move toward that tree. 

Additionally, all birds were programmed with a sense of affinity toward other birds. For instance, bird A would would approach other birds that were in its field of vision, if bird A was not too hungry. Otherwise, it would prioritize food. Each bird also had an individual lifespan and maximum speed at which it could move.  

For translating the simulation into audio, the linear velocities of birds were used to control band-pass filters applied to white noise. In contrast to the grid-based input of the previous simulation, this approach allowed for a range of outputs beyond prescribed musical tones. This was a decision made to test the system’s capacity for diverse outputs, in line with the goal of a high barrier for exploration. By interpreting the state of a bird (its velocity) that is the result of internal goals (to seek food or peers), we also begin to see layers of meaning established, although admittedly not ready to be manipulated. Succinctly, if our first exercise was one that leaned heavily on exploring user inputs, this example was meant to examine the benefits and possibilities of layering complexity within a system. 

When the simulation is run, we hear noise (as to be expected) as the birds forage. Their foraging movement is semi-random, and as such, the frequencies of the corresponding band-pass filters are shifted back and forth. However, periodically, we hear certain frequencies—perhaps not unlike the sound effects of a cartoon flying saucer—as certain behaviors happen. As birds accelerate, their increasing velocity as they move across the plane to food or peers results in the band-pass filters shifting up in frequency, in tandem. 

| <iframe width="640" height="480" src="https://www.youtube.com/embed/nLHWwPOA41k" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> |
| :----------------------------------------------------------: |
| **Video 3**: An early implementation of an agent-based system. Each blue dot ("bird") represents a white noise generator. The X- and Y-velocity of each bird is mapped to band-pass filters that change the noise to a discernible frequency as birds move toward a particular point. |

It is important to note that the parameter of each bird that is mapped to sound, its linear velocity in the X- and Y- directions, was selected for its visibility. That is, unless we have a way of illustrating or gleaning information about internal states of the birds, we cannot interpret the multiple layers of meaning. While we can surmise some details about a bird’s target based on the trajectory of its movements, the translation of movement into sound means that we only hear the resultant action of an internal state, rather than the process of state change or the relationship between states. 

We might also argue that when we turn our attention to the information gleaned from this system’s audio, we find that most of the audio output contains little or no meaning. In this simulation, a bird’s foraging movements are random until it sees a tree to eat or another bird to join, so the filter it applies to the white noise generation is accordingly random. While aggregate randomness could be said to be a form of information—that is, we can glean from unstructured noise that no bird is moving toward a target—that information is still muddled by the fact that it is similarly transformed by separate events. A bird approaching food and approaching another bird, or several birds flocking together, perform essentially the same transformation on the audio. Additionally, a flock of birds that might emerge is a notable event within the system, but cannot be conveyed because its constituent birds continue to move mostly randomly, albeit in close proximity to each other. The individual corrective actions taken to maintain a flock are quite small, so the accompanying audio cues are not perceived as the flock moves. 

This desire to transform the events of our system into discrete, informative sounds is reminiscent of what Damian Keller and Barry Truax called an ecological approach to composition. They argued that rather than using prescriptive musical forms, “time be parsed into informationally relevant events … attention-based processes are triggered by organized transformation, not by redundancy or randomness.” <a href="references.md#keller">[37]</a> Keller presented his ecological approach to composition in connection to J.J. Gibson's notion that "information is structure that specifies an environment to an animal." <a href="references.md#keller2">[38]</a> A loop forms between environment, information and animal, and this gives rise to compositional structure. "Actually occurring events" form the basis of the structure rather than arbitrary measures of time, and as a result, change "is not simply the fluctuation of variables: it dictates how these variables are observed."  <a href="references.md#keller">[37]</a>

In our simulation, we can see an event-driven relationship between agents and the environment as birds move, trees multiply, and birds eat. The changes in the environment and resultant actions of birds gives rise to the structure of the sound. But if we analyze this system in reference to the computer as instrument, we see that we have decoupled the musician's interpretation of informative events from a method of input to the system. If we want to engage all four points of the previously described framework, we might then focus on McCormack’s idea of “interactive evolution” through “environmental modification,” and allowing a user to impart their intentions back into the system using knowledge gleaned from the system. <a href="references.md#mccormack">[30]</a> But what would this look like? 

#### Audion

For the next (and, with respect to this writing, final) iteration of Audion, the focus was incorporating all four of the stated design criteria, and conveying information between the user and the system and developing the expressive capabilities of the program. 

This iteration was an expansion of the previous birds-and-trees metaphor, however the trees were removed as separate agents. At startup, the user is  once again presented a two-dimensional space with an initial  group of birds whose parameters are randomly instantiated. Each bird has three internal states: fear, hunger and desire to mate. The level of each desire (assigned between one and 100) fluctuates with time and in response to environmental variables that the user can manipulate. Each bird’s internal states are also given individual weights, so some might respond more strongly to fear than hunger, etc. Each bird also has a gender, maximum speed, a lifespan and a field of vision that are assigned within a normal distribution. 

Each state has a corresponding, discrete action. When a bird reaches its threshold for hunger and a piece of food is within its field of vision, it eats. When a bird reaches its threshold for desire to mate and can “see” another bird, it moves to mate. When a bird reaches its threshold for fear, it will “flee.” Eating and mating return the levels of hunger and desire to 0, respectively. When a bird flees, its level of fear decreases in proportion to the distance between it and the threat. 

In order to manipulate the environment and affect the birds accordingly, the user can place four different object types into the simulation. Each object has a radius of efficacy, and each object can be connected to an audio file for sampling grains. The grains are triggered by a bird eating, mating or fleeing within the object's radius. The four objects and their functions are explained in **Table 1**. 

<table>
    <tr>
        <caption><strong>Table 1</strong>: User-placed Objects in Audion</caption></tr>
    <tr>
    	<td><strong>Object</strong></td><td><strong>Icon</strong></td>
    </tr>
    <tr>
    	<td>The <strong>"feeder"</strong> object disperses the pieces of food that the birds need to survive, and its audio is triggered by birds eating.</td>
    	<td><img src="./media/photos/feeder-black.png" style="zoom:75%"></td>
    </tr>
    <tr>
    	<td>The <strong>“watcher”</strong> object accelerates the increase of a bird’s desire to mate, and its audio is triggered when a pair of nearby birds mates.</td>
    	<td><img src="./media/photos/watcher-black.png" style="zoom:75%"></td>
    </tr>
    <tr>
    	<td>The <strong>"hunter"</strong> object increases a bird’s level of fear, and its audio is triggered when a bird flees from it.</td>
    	<td><img src="./media/photos/hunter-black.png" style="zoom:75%"></td>
    </tr>
    <tr>
    	<td>The <strong>"plauge"</strong> object has no effect on the states of individual birds and produces no sound directly, but a percentage of birds are killed when they linger in its effective radius.</td>
    	<td><img src="./media/photos/plague-black.png" style="zoom:75%"></td>
    </tr>    
</table>

#### Motivation for the use of granular synthesis 

We mentioned the work of Truax and Keller in analyzing the relationship between environmental event and audio event within the previously described agent-based simulation, and their work with granular synthesis heavily influenced the final proposal for Audion. Granular synthesis is the production of "complex sounds ... based on the production of a high density of acoustic events called 'grains.'" <a href="references.md#truax">[39]</a> In the case of Audion, these grains are drawn from user-selected audio files. 

Manipulating grains of audio allows for the transformation of sound on multiple time scales. Curtis Roads wrote of the "macro," "meso" and "micro" time scales of music, which we also see in the work of Truax and Keller. <a href="references.md#roads">[37, 40]</a> An individual grain, typically measured in milliseconds, represents the micro time scale. The meso time scale represents collections of sounds into phrases, and the macro time scale encompasses larger structures of musical forms. A micro-scale grain has qualities imparted by its audio source and amplitude envelope, and the combination of grains can be manipulated to create rhythmic patterns, tonality and noise on the meso time scale. <a href="references.md#roads">[40]</a>

Truax and Keller used physical models derived from natural events to control grains on the meso and macro time scales, and they wrote that through "interaction of the local waveforms with meso-scale time patterns …  the [audio] output is characterized by the emergent properties, which are not present in either global or local parameters.” <a href="references.md#keller">[37, 38]</a> Notably, Truax attempted to evoke the physical movement of water when designing a system for controlling grains in his 1986 composition *Riverrun*: 

> “Riverrun creates a sound environment in which stasis and flux, solidity and movement co-exist in a dynamic balance … The fundamental paradox of granular synthesis—that the enormously rich and powerful textures it produces result from its being based on the most ‘trivial’ grains of sound—suggested a metaphoric relation to the river whose power is based on the accumulation of countless ‘powerless’ droplets of water.” <a href="references.md#riverrun">[39]</a>

Likewise, we could see a metaphorical connection between granular synthesis and the foraging birds of our model. Any single agent, in response to the system’s configuration and user inputs, triggers its representative grain, but the character of the individual and combined grains changes over time. By recombining and manipulating existing sounds, we generate new music, but the added possibility for interaction and real-time adjustment, as well as the unpredictable motion of agents, might ideally create a system for highly varied explorations. 

#### Interaction Design

While the sonic component of any audio environment is obviously important, we must also address how directly it engages the proposed design criteria. 

First, in order to allow a user to steer but not directly control the system, the user-placed objects addressed a specific parameter of the individual birds, but also the system-wide movement. By amplifying the birds' attraction, for instance, we increase the total number of birds, and thus the number of grains that might be played simultaneously. Higher levels of mating desire also increase the pitch of the grain played. Mating among the birds, however, passes along the “mother’s” attributes to the offspring, within a certain allowance for random mutation. The ultimate result is that, depending on the combination of birds’ attributes, as well as factors like the availability of food and the number of user-placed agents in the plane, we wind up with complex interactions that can be foreseen but not produced with absolute certainty. 

Second, we should consider the levels of information available within the system and our ability to manipulate them. The system has at least three levels of control for users to interact with: the individual bird and its states, the interaction between agents in the system (bird-and-bird, bird-and-object, bird-and-food), and the ecosystem’s properties as a whole (population, object placement). 

It is also important that each of these levels can be interpreted and manipulated. To convey how the bird will interact with other agents, we can interpret its field of vision, dominant state and gender. The field of vision indicates whether or not a bird “sees” a bird or object to encounter, and its dominant state will determine how it responds. To this end, the bounding shape of a bird reflects its motivating internal state, as well as the boundaries of its field of vision; the inner shape of a bird indicates its gender. 

To change the system’s population level, we can place watcher or plague objects, and, to some degree, control the number of birds present through general trends of growth and death. The relative abundance of food can also lead to population decay or stability. 

Third, we should consider the system’s adaptability. While there is no explicit fitness function, as is common in many evolutionary algorithms, the competitive aspects of the simulation lead to some forms of evolution. Because much of a bird’s ability to interact is dictated by its field of vision, over time, the simulation tends to produce birds with larger fields of vision. Similarly, because faster “female” birds tend to move between “males” more quickly to mate, there is typically an increase in the average speed of birds. 

#### Expressive Quality

Finally, we should consider the ways in which a user can express musical ideas with the tools available in the system. Audion was designed in such a way that the user can shape facets of the timing, pitch and timbre of the audio output, but that is not to say the examination offered here is an exhaustive list of possibilities. For a discussion of manipulating these qualities, we will ignore the user's choice of audio file from which grains are extracted because this choice&mdash;while important&mdash;is arbitrary, and Audion's agents will behave similarly no matter the file selected. 

| <iframe width="640" height="480" src="https://www.youtube.com/embed/DOavzVKyExo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> |
|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **Video 4**: A demonstration of the use of Audion and some of its expressive features.                                                                                                                     |

Perhaps the most apparent way to control timing in Audion is by controlling the discrete actions that trigger sound grains. How this is achieved depends on the types of objects a user has placed in the environment. For instance, when a bird eats, it triggers the grains of a respective feeder object, so we are able to trigger those grains more frequently by manipulating the radius of the feeder object and/or the number of "seeds" it drops as food.  Similarly, the radii of the watcher and hunter objects contribute to the frequency with which their grains are triggered. 

If we consider the perceived movement of the audio through panning as an expression of timing (i.e. movement is a change predicated upon time), we can also manipulate the timing through the X-positions of objects in the field. The sound produced by an object is panned relative to the bird triggering the grain. For instance, if a pair of birds mates to the left of a watcher object, the sound triggered will be panned proportionally to the left.

Pitch control also has multiple methods for manipulation. Similar to the way in which the X-positions of a user-placed object control the panning as birds act near it, the object's Y-position manipulates the pitch of a grain, with a lower Y-position (with the origin in the upper left of the field) corresponding to a lower pitch. 

Some sound parameters are meant to have a level of intuitiveness. Higher levels of attraction and fear for individual birds means the grains they trigger will be more erratic: a bird with a higher desire to mate will randomly select from a wider range of possible values to shift the pitch of the grain. Hunger is also intended to have an intuitive meaning: a bird with a higher level of hunger will trigger a grain with a longer duration. 

Modifying the system's output as a whole is done with aggregate, system-wide variables derived from all the birds. The average hunger of the population controls the system's level of reverberation, with a hungrier population leading to a more lively reverberation. The average desire to mate controls a resonance filter applied to the audio output. As the average desire to mate increases in the simulation, the cutoff frequency of the resonance filter also increases. 

From here, we can make the connection between grain size and the previously discussed manipulation of timing: The layering of grains and resultant polyphony emerge as a function of both actions taken by individual birds and the system-wide variables affected by the objects the user has placed in the simulation. This circular connection is part of the intent behind Audion. Much like the use of granular synthesis, this feedback loop between objects, agents and states acting at various levels within the system is intended to evoke Keller's notion that time in composition is derived not from time as independent from "actually occurring events," but rather as dependent on events happening in relation to each other. However, as Keller notes, an important element of this ecological model for composition "is the organization of spectrally complex samples into feasible meso-temporal patterns." These "meso and macro" patterns should then "\[unveil\] new properties resulting from the interaction of these levels." <a href="references.md#keller">[37]</a>

**[V. Evaluation  >>](partFive.md)**