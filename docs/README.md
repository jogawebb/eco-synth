## Eat Your Grains: Artificial Ecosystems, Granular Synthesis and Generative Music

---

By [Gabe Webb](https://noiseandnumbers.com)

Thesis Advisors Edwin van der Heide, Rob Saunders 

Graduation Thesis

Media Technology M. Sc. 

Leiden University 

June 2020

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->

<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

### Abstract

Composers and scientists have used algorithms to create new pieces of music for centuries, and they have often been inspired by natural phenomena for modeling their work. The advancement of recording technology and the development of the digital computer in the mid-to-late 20th century allowed musicians, artists and researchers to apply statistical models derived from natural sciences to musical composition and manipulate sounds on timescales that were previously impossibly small.  

In this work, we propose Audion, a multi-agent software system for granular synthesis and real-time musical exploration. First, we provide the historical, artistic and scientific contexts for current approaches in generative music, paying special attention to methods inspired by cellular automata, multi-agent systems and artificial ecosystems. We then discuss the development of Audion, drawing from design criteria derived from the work of Gordon Pask.

 Audion allows a user to interact with a field of “birds” by placing digital objects in an on-screen environment. These objects modify internal states of the birds: hunger, fear and attraction. Individual birds and their internal states trigger and modify individual grains of audio, and ecosystem-wide variables control parameters of the resultant granular sound. The audio output references the ecologically inspired granular synthesis approach of Barry Truax and Damian Keller. Based on the stated criteria and provided context, we suggest improvements for Audion, such as methods for applying the simulation's emergent structures to audio output and creating environmental pressures in the simulation that are independent of the user's manipulations. 

**[I. An Overture, of Sorts  >>](partOne.md)**

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

----

Forked from @virtuacreative

[ci]: https://about.gitlab.com/gitlab-ci/
[GitBook]: https://www.gitbook.com/
