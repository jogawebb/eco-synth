## References 

| [#]  | Source |
| ---- | ------ |
| <a id='eno'>[1]</a> | Brian Eno, “Generative Music: A Talk Delivered in San Francisco, June 1996.” In Motion Magazine (July 1996). |
|<a id='eno2'>[2]</a> | Brian Eno, *Ambient 1: Music for Airports*. PVC (1978). |
|<a id='lysaker'>[3]</a> | John Lysaker, “Turning Listening Inside Out: Brian Eno’s Ambient 1: Music for Airports.” *The Journal of Speculative Philosophy* 31 No. 1 (2017): 155-176. |
|<a id='conway'>[4]</a> | User liambdonegan01, “Conway’s Game of Life.” Wikipedia. Accessed 5 March 2020. |
|<a id='collins'>[5]</a> | Nick Collins, "The Analysis of Generative Music Programs." *Organised Sound* 13 No. 3 (2008): 237-248. |
|<a id='pesic'>[6]</a> | Peter Pesic, *Music and the Making of Modern Science*. Cambridge, Massachusetts: The MIT Press (2014). |
|<a id='shehadi'>[7]</a>| Fadlou Shehadi, *Philosophies of Music in Medieval Islam*. Leiden, The Netherlands: E.J. Brill (1995). |
|<a id='serquera'>[8]</a> | Jaime Serquera, Eduardo Reck Miranda, "Histogram Mapping Synthesis: Cellular Automata-based Technique for Flexible Sound Design." *Computer Music Journal* 38 No. 4 (Winter 2014): 38-52. |
|<a id='spasov'>[9]</a> | Miroslav Spasov, “Music Composition as an Act of Cognition: ENACTIV—Interactive Multi-Modal Composing System.” *Organised Sound* 16 No. 1 (2011): 69-86. |
|<a id='puckette'>[10]</a> | CIRMMT, "Miller Puckette - Design choices for computer instruments and computer compositional tools." YouTube. Video. Accessed 20 July 2020. |
|<a id='swift'>[11]</a> | Johnathan Swift, *Gulliver’s Travels*. Project Gutenberg website. Accessed 12 Nov. 2019. |
|<a id='selfridge'>[12]</a> | Eleanor Selfridge-Field, “Composition, Combinatorics and Simulation: A Historical and Philosophical Enquiry.” *Virtual Music: Computer Synthesis of Musical Style*. Ed. David Cope (2001). |
|<a id='weisstein'>[13]</a> | Eric Weisstein, “Combinatorics.” MathWorld—A Wolfram Web Resource. Accessed 15 April 2020. |
|<a id='essl'>[14]</a> | Karlheinz Essl, “Algorithmic Composition.” *The Cambridge Companion to Electronic Music*. Ed. Nick Collins (2007). |
|<a id='parvianinen'>[15]</a> | Tero Parvianinen, “How Generative Music Works: A Perspective.” Blog. Accessed 4 March 2020. |
|<a id='kozinn'>[16]</a> | Allan Kozinn, “A Classic Minimalist Score, Played at Maximal (and Electronical) Length.” *The New York Times*. 10 Nov. 2009. |
|<a id='huizenga'>[17]</a> | Tom Huizenga, “Fifty Years of Steve Reich’s ‘It’s Gonna Rain.’” National Public Radio website. Accessed 17 March 2020. |
|<a id='riley'>[18]</a> | Terry Riley, Ictus, Blindman Kwartet, *In C: Terry Riley*. Cyprès Records: 2000. |
|<a id='riley2'>[19]</a> | Terry Riley, Members of the Center of the Creative and Performing Arts in the State University of New York at Buffalo, *Terry Riley: In C*. CBS Records: 1968. |
|<a id='ames'>[20]</a> | Charles Ames, “The Markov Process as a Compositional Model: A Survey and Tutorial.” *Leonardo* 22 (1989): 175-187. |
|<a id='cage'>[21]</a> | “Imaginary Landscape No. 4 (March No. 2).” The John Cage Trust website. Accessed 6 March 2020. |
|<a id='herremans'>[22]</a> | Dorien Herremans, Ching-Hua Chan, Elaine Chew, “A Functional Taxonomy of Music-generation Systems.” *ACM Computing Surveys* 50 No. 5 (Sept. 2017). |
|<a id='cohen'>[23]</a> | Joel E. Cohen, “Information Theory and Music.” *Behavioral Science* 7 No. 2 (1962): 137-164. |
|<a id='macchia'>[24]</a> | Sebastian Macchia, “Making an Album with Music Transformer.” Magenta blog. Accessed 8 March 2020. |
|<a id='temperly'>[25]</a> | David Temperly, *Music and Probability*. Cambridge, Massachusetts: The MIT Press (2007). |
|<a id='siddique'>[26]</a> | Nzamul Siddique, Jojja Adeli, “Nature-inspired Computing: An Overview and Some Future Directions.” *Cognitive Computing* No. 7 (2015): 706-714. |
|<a id='schacher'>[27]</a> | Jan C. Schacher, Daniel Bisig, Philippe Kocher. “The Map and the Flock: Emergence in Mapping with Swarm Algorithms.” *Computer Music Journal* 38 No. 3 (Fall 2014): 49-63. |
|<a id='maximos'>[28]</a> | Maximos A. Kaliakatsos-Papakostas, Andreas Floros, Michael N. Vrahatis, "Interactive Music Composition Driven by Feature Evolution.” *SpringerPlus* No. 5 (2016). |
|<a id='oram'>[29]</a> | Daphne Oram, *An Individual Note of Music, Sound and Electronics*. London: Galliard Ltd. (1972). |
|<a id='mccormack'>[30]</a> | John McCormack, “Creative Ecosystems.” *Computers and Creativity*. Ed. John McCormack and Mark d'Inverno (2012). |
|<a id='gras'>[31]</a> |Robin Gras, Didier Devaurs, Adrianna Wozniak, Adam Aspinall. “An Individual-based Evolving Predator-prey Ecosystem Using a Fuzzy Cognitive Map as the Behavior Model.” *Artificial Life* 15 No. 4 (2009): 423-463. |
|<a id='pask'>[32]</a> | Gordon Pask, “A Comment, a Case History and a Plan.” *Cybernetic Serendipity*. Ed. Jasia Reichardt (1970). |
|<a id='nagle'>[33]</a> | Paul Nagle, "Yamaha Tenori-On." *Sound on Sound* (Feb. 2008). |
|<a id='lem'>[34]</a> |Rich Lem, “Tenori-On.” Wikipedia. Accessed 8 June 2020. |
|<a id='love'>[35]</a> | LÖVE, love2d.org. |
|<a id='puredata'>[36]</a> | Pure Data, puredata.info. |
|<a id='keller'>[37]</a> |Damian Keller, Barry Truax, “Ecologically Based Granular Synthesis.” International Computer Music Conference Proceedings (1998). |
|<a id='keller2'>[38]</a> | Damian Keller, "Compositional Processes from an Ecological Perspective." *Leonardo Music Journal* 10 (2000): 55-60. |
|<a id='truax'>[39]</a> | Barry Truax, *Riverrun*. Simon Fraser University website. Accessed 10 June 2020. |
|<a id='roads'>[40]</a> | Curtis Roads, *Microsound*. Cambridge, Massachusetts: The MIT Press (2001). |