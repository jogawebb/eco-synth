## VI.  Conclusion

Audion is an attempt to create a system that is distinct from conventional instruments by disrupting the connection between a particular user input and a predictable output. While there are a plethora of historical and contemporary examples of generative systems, Audion is distinguished by a focus on real-time exploration, the use of granular synthesis, and a mode of interaction based on manipulating the environment of an ecologically inspired simulation. 

Audion's development began with a series of experiments focused on translating the activity of agent-based systems into a musical tool. Agent-based systems were chosen because of their ability to transform relatively little user input into high levels of output, and they do so while potentially offering emergent structures that are useful for musical expression. The use of state-motivated agents with goals independent of the musician's goals presents an interesting level of interactivity. The incorporation of granular synthesis was inspired by the work of Truax and Keller, and the writing of Roads, which drew connections between units of sound and various timescales of musical information. 

In order to produce desirable results in the realm of music, a user must also understand Audion's simulation. By disrupting the execution of a musical idea, we attempt to invite the musician using Audion to correct, incorporate or oppose the system's output. This co-evolution is the crux of the system. 

The focus of further development should become the clarity between user action and system behavior, so that the user can make more informed choices to affect the audio output. Additionally, the system would benefit from more precise manipulation of timescales. As an open-ended instrument, Audion can be effective at producing interesting sounds, but the use of the emergent structures of the system's agents is underutilized. 

**[Acknowledgments  >>](acknowledgments.md)**