-- Import required files.
require("src/bird")
require("src/observer")
require("src/seed")
require("src/interface")
require("src/functions")
require("src/gooi")

-- Imports for network communication.
local socket = require("socket")

print(love.system.getOS())

-- Initial window size.
WIDTH = 1200
HEIGHT = 1000

-- sets dragging and selection for mouse adjustments.
local isDragging = false
local beingDragged = nil


isSelected = nil
popupOpen = false
popupDrag = false
currentVolume = 1
masterFear = 0
dspON = false

loveFont = love.graphics.newFont("src/assets/fonts/MavenPro-VariableFont_wght.ttf", 120, "normal", 8)

-- Variables to control overall refresh rate.
t = 0
fps = 60

-- Starting Variables
startBirds = 5
maxBirds = 50
minBirds = 5
birthrate = 0.75

-- Arrays to hold our agents.
birds = {}
seeds = {}
observers = {}

-- Files loaded to be samples.
loadedFiles = {}
fileNames = {}

-- Create a world for Love2D.
ecosystem = love.physics.newWorld(0, 0, true)

-- Set IP and port for communication to pd.
local address, port = "127.0.0.1", 8084

function love.load()

  -- Load media assets.
  loadSamples()
  loadImages()

  -- Load interface elements.
  loadInterface()
  loadContextMenu()

  -- Set window parameters.
  love.window.setMode(WIDTH, HEIGHT, {resizable = true})

  -- Set network parameters.
  udp = socket.udp()
  udp:settimeout(0)
  udp:setpeername(address, port)

  -- Create body for the mouse pointer.
  mouseBody = love.physics.newBody(ecosystem,
    love.mouse.getX(),
    love.mouse.getY(),
    "static")
  mouseShape = love.physics.newCircleShape(20)
  mouseFixture = love.physics.newFixture(mouseBody, mouseShape, 1)

  -- Initial array of birds.
  for i=1, startBirds do
    newBird = Bird:new()
    newBird:init()
    table.insert(birds, newBird)
  end

  udp:send("masterVolume " .. 1)

end

function love.update(dt)

  -- Update time.
  t = t + dt

  -- Do checks for statuses and collisions every 1/fps seconds.
  if t >= 1 / fps then

    -- Update mouse position
    mouseBody:setX(love.mouse.getX())
    mouseBody:setY(love.mouse.getY())

    for i=table.getn(birds), 1, -1 do
      birds[i]:update()
      birds[i]:checkCollisions()
      if birds[i].offSpring > 3 then
        table.remove(birds, i)
      elseif birds[i].age > birds[i].lifeSpan then
        table.remove(birds, i)
      elseif birds[i].timeSinceFood >  60 - (birds[i].hungerFactor * 6) then
        table.remove(birds, i)
      end
    end

    if table.getn(observers) > 0 then
      for i=table.getn(observers), 1, -1 do
        observers[i]:update()
        observers[i]:sendData(i)
        print(i, observers[i].index, observers[i].file)
      end

    end

    for i=table.getn(seeds), 1, -1 do
      seeds[i]:update()
      if seeds[i].age > 10 then
        table.remove(seeds, i)
      end
    end

    -- Generate new birds if the total population falls below the set minimum.
    if table.getn(birds) < minBirds then
      for i=minBirds - table.getn(birds), 1, -1 do
        newBird = Bird:new()
        newBird:init()
        table.insert(birds, newBird)
      end
    end
    t = 0
    masterAttributes()
  end


  -- Update the love2d ecosystem.
  ecosystem:update(0.1)

  -- Update Gooi elements
  gooi.update(dt)

end

function love.draw()

  drawMouse()

  -- Draw boids, seeds and observers
  love.graphics.setBackgroundColor(18/255, 18/255, 18/255, .9)

  for i=table.getn(birds), 1, -1 do
    birds[i]:display()
  end
  for i=table.getn(seeds), 1, -1 do
    seeds[i]:display()
  end
  for i=table.getn(observers), 1, -1 do
    observers[i]:display()
  end

  gooi.draw()

  if popupOpen == true then
    gooi.draw("context")
    if isSelected.type == "F" then
      gooi.draw("feeder")
    end
  end
end

function love.filedropped(file)
  local theFile = file:getFilename()
  local fileAlias = string.match(theFile, "^.+/(.+)$")
  loadedFiles[fileAlias] = theFile
  table.insert(fileNames, fileAlias)

  filePicker.max = table.getn(fileNames)

end

function love.resize(w, h)
  WIDTH = w
  HEIGHT = h
end

function love.mousepressed(x, y, button, isTouch, presses)
  if button == 1 and popupOpen == true then
    local checkX = love.mouse.getX()
    local checkY = love.mouse.getY()
    if checkX > panelLabel.x
    and checkX < panelLabel.x + panelLabel.w
    and checkY > panelLabel.y
    and checkY < panelLabel.y + panelLabel.h then
      popupDrag = true
    end
  end
  if table.getn(observers) > 0 then
    if button == 1 then
      for i=table.getn(observers), 1, -1 do
        if love.physics.getDistance(mouseFixture, observers[i].fixture) < 15
        and popupOpen == false then
          isDragging = true
          beingDragged = observers[i].body
        end
      end
    elseif button == 2 then
      for i=table.getn(observers), 1, -1 do
        if love.physics.getDistance(mouseFixture, observers[i].fixture) < 10 then
          isSelected = observers[i]
          updateInterface()
          popupOpen = true
        end
      end
    end
  end
  gooi.pressed()
end

function love.mousereleased(x, y, button, isTouch, presses)
  gooi.released()
  isDragging = false
  popupDrag = false

end

function love.mousemoved(x, y, dx, dy, isTouch)

  -- Update the position of the mouse.
  mouseBody:setX(x)
  mouseBody:setY(y)

  -- Reposition elements.
  if isDragging == true then
    beingDragged:setX(x)
    beingDragged:setY(y)
  end

  -- Update the interface if the user is dragging a pop-up window.
  if popupDrag == true then
    updateInterface()
  end

end

function love.keypressed(key, scancode, isrepeat)
  if key == "f" or key == "w" or key == "h" or key == "v" then
    local newFeeder = Observer:new()
    newFeeder:init()
    newFeeder.type = string.upper(key)
    table.insert(observers, newFeeder)
    newFeeder.index = table.getn(observers) - 1
  end
end
