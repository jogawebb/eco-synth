Bird = {
  body = 0,
  shape = 0,
  fixture = 0,
  fearFactor = 0,
  hungerFactor = 0,
  desireFactor = 0,
  sight = 0,
  speed = 0,
  angle = 0,
  fear = 0,
  hunger = 0,
  desire = 0,
  gender = 0,
  direction = math.random(1, 8),
  steps = 0,
  offSpring = 0,
  timeSinceMate = 0,
  timeSinceFood = 0,
  age = 0,
  lifeSpan = 0
}

function Bird:new (o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end

function Bird:init()

  self.body = love.physics.newBody(ecosystem, WIDTH/2, HEIGHT/2, "dynamic")
  self.shape = love.physics.newCircleShape(12)
  self.fixture = love.physics.newFixture(self.body, self.shape, 1)
  self.fearFactor = math.random(50, 100) / 100
  self.hungerFactor = math.random(100) / 100
  self.desireFactor = math.random(100) / 100
  self.sight = math.random(50, 100)
  self.speed = math.random(25)
  self.angle = 0
  self.fear = 0
  self.hunger = 0
  self.desire = 0
  self.gender = math.random(0, 100) % 2
  self.lifeSpan = love.math.randomNormal(3, 30)
  self.direction = math.random(1, 8)

end

function Bird:checkCollisions()
  for i=table.getn(seeds), 1, -1 do
    if self.body:isTouching(seeds[i].body) then
      table.remove(seeds, i)
      for j=table.getn(observers), 1, -1 do
        local observer = observers[j]
        local d = love.physics.getDistance(self.fixture, observer.fixture)
        if d <= observer.radius then
          if observer.type == "F" then
            self:sendData(observer)
            self.hunger = 0
            self.timeSinceFood = 0
          end
        end
      end
    end
  end
  for i = table.getn(birds), 1, -1 do
    if self.body:isTouching(birds[i].body) and self.gender ~= birds[i].gender then
      -- Hatch a baby.
      if math.random() > birthrate then
        if table.getn(birds) <= maxBirds then
          self:mate()
        end
        for j=table.getn(observers), 1, -1 do
          local observer = observers[j]
          local d = love.physics.getDistance(self.fixture, observer.fixture)
          if d <= observer.radius and observer.type == "W" then
            self:sendData(observer)
            self.desire = 0
          end
        end
      end
    end
  end
end

function Bird:mate()
  if self.timeSinceMate > 5 then
    newBody = love.physics.newBody(ecosystem,
      self.body:getX() + math.random(-3, 3),
      self.body:getY() + math.random(-3, 3),
      "dynamic")
    newBird = Bird:new()
    newBird.body = newBody
    newBird.shape = love.physics.newCircleShape(12)
    newBird.fixture = love.physics.newFixture(newBird.body, newBird.shape, 1)
    newBird.fearFactor = self.fearFactor
    newBird.hungerFactor = self.hungerFactor
    newBird.desireFactor = self.desireFactor
    newBird.sight = self.sight
    newBird.speed = self.speed
    newBird.gender = math.random(0, 1)
    newBird.direction = math.random(1, 8)
    newBird.steps = 0
    newBird.lifeSpan = self.lifeSpan
    table.insert(birds, newBird)
    self.offSpring = self.offSpring + 1
    self.timeSinceMate = 0
  end
end

function Bird:update()

  self.timeSinceMate = self.timeSinceMate + love.timer.getDelta()
  self.timeSinceFood = self.timeSinceFood + love.timer.getDelta()
  self.age = self.age + love.timer.getDelta()

  -- Set default seed value just out of sight range, check seeds.
  local distanceToSeed = self.sight + 1
  local seed = nil
  if table.getn(seeds) > 0 then
    -- local seed = seeds[1]
    for i=table.getn(seeds), 1, -1 do
      local d = love.physics.getDistance(self.fixture, seeds[i].fixture)
      if d < distanceToSeed then
        distanceToSeed = d
        seed = seeds[i]
      end
    end
  end

  -- Check distance to other birds.
  local distanceToMate = self.sight + 1
  local mate = birds[1]
  for i=table.getn(birds), 1, -1 do
    local m = love.physics.getDistance(self.fixture, birds[i].fixture)
    if m < distanceToMate and m > 0 then
      distanceToMate = m
      mate = birds[i]
    end
  end

  -- Calculate fear based on closest observer.
  if table.getn(observers) > 0 then

    -- Check distance to Observers.
    local distanceToObserver = self.sight + 1
    local observer = observers[1]
    for i=table.getn(observers), 1, -1 do
      local b = love.physics.getDistance(self.fixture, observers[i].fixture)
      if b < distanceToObserver then
        distanceToObserver = b
        observer = observers[i]
      end
    end

    -- Calculate fear.
    self.fear = self.fear + ((self.sight - distanceToObserver) / self.sight)

    -- Avoid an observer.
    if distanceToObserver  <= self.sight then
      self.fear = self.fear + (self.sight - distanceToObserver) * 0.1
      if distanceToObserver < self.sight * self.fearFactor  then
        local x = -(observer.body:getX() - self.body:getX())
        local y = -(observer.body:getY() - self.body:getY())
        self.desire = self.desire - self.fearFactor
        self.hunger = self.hunger + self.fearFactor
        self.body:applyForce(x, y)
        if observer.type == "H" then
          self:sendData(observer)
        end
      end

    else
      self.fear = self.fear - 1.0
    end

  end

  -- Calculate hunger and desire.
  self.hunger = self.hunger + self.hungerFactor + (self.fear * 0.005)
  self.desire = self.desire + self.desireFactor - (self.fear * 0.005)

  local distanceToMouse = love.physics.getDistance(mouseFixture, self.fixture)
  if distanceToMouse <= (self.sight * self.fearFactor) then
    local x = -(love.mouse.getX() - self.body:getX())
    local y = -(love.mouse.getY() - self.body:getY())
    self.desire = self.desire - self.fearFactor
    self.hunger = self.hunger + self.fearFactor
    self.body:applyForce(x, y)
  end

  -- Go toward a seed if hungry.
  if distanceToSeed <= self.sight then
    if self.hunger >= self.hungerFactor * 100
    and self.fear < (self.fearFactor * self.sight) then
      local x = (seed.body:getX() - self.body:getX())
      local y = (seed.body:getY() - self.body:getY())
      if x > self.speed then
        x = self.speed
      elseif x < -self.speed then
        x = -self.speed
      end
      if y > self.speed then
        y = self.speed
      elseif y < -self.speed then
        y = -self.speed
      end
      self.body:applyForce(x, y)
    end
  end

  -- Move towards mate.
  if distanceToMate <= self.sight then
    if self.desire > self.desireFactor * 100
    and self.fear < self.fearFactor * self.sight then
      local x = (mate.body:getX() - self.body:getX())
      local y = (mate.body:getY() - self.body:getY())
      if x > self.speed then
        x = self.speed
      elseif x < -self.speed then
        x = -self.speed
      end
      if y > self.speed then
        y = self.speed
      elseif y < -self.speed then
        y = -self.speed
      end
      self.body:applyForce(x, y)
    end
  end

  -- Walk if it doesn't see a seed.
  if distanceToSeed > self.sight
  and distanceToMate > self.sight
  and self.fear < (self.fearFactor * self.sight) then
    local x = math.cos(self.angle) * math.random(self.speed)
    local y = math.sin(self.angle) * math.random(self.speed)
    self.body:setLinearVelocity(x, y)
    self.steps = self.steps + 1
  end

  -- Constain x-position to window size.
  if self.body:getX() > WIDTH then
    self.body:setX(self.body:getX() - WIDTH)
  elseif self.body:getX() < 0 then
    self.body:setX(self.body:getX() + WIDTH)
  end

  -- Constrain y-position to window size.
  if self.body:getY() > HEIGHT then
    self.body:setY(self.body:getY() - HEIGHT)
  elseif self.body:getY() < 0 then
    self.body:setY(self.body:getY() + HEIGHT)
  end

  -- Change direction after 1000 steps.
  if self.steps > 1000 then
    self.direction = math.random(8)
    self.steps = 0
  end

  -- Set values for hunger, fear and desire to range of 0 to 100.
  if self.hunger < 0 then
    self.hunger = 0
  elseif self.hunger > 100 then
    self.hunger = 100
  end
  if self.desire < 0 then
    self.desire = 0
  elseif self.desire > 100 then
    self.desire = 100
  end
  if self.fear < 0 then
    self.fear = 0
  elseif self.fear > 100 then
    self.fear = 100
  end

end

function Bird:sendData(observer)
  local d = love.physics.getDistance(self.fixture, observer.fixture)
  udp:send(observer.index .. " grainSize " .. math.max(self.hunger * 5, 10))
  udp:send(observer.index .. " pitch " .. (self.body:getY() / HEIGHT) * 12 + (self.desire / 4.125))
  udp:send(observer.index .. " pitchRandom " .. self.desireFactor)
  udp:send(observer.index .. " volume " .. d / observer.radius)
  local pan = (self.body:getX() / WIDTH)
  udp:send(observer.index .. " panning " .. pan)
  udp:send(observer.index .. " panRandom " .. self.fear)
  udp:send(observer.index .. " jumpRandom " .. self.fearFactor)
  observer.startPoint = observer.startPoint + (self.fear / 10000)
  udp:send(observer.index .. " startPoint " .. observer.startPoint)
  udp:send(observer.index .. " masterFear " .. masterFear)
  udp:send(observer.index .. " grainDist " .. (table.getn(birds) / ((WIDTH / 100) * (HEIGHT / 100))) * 100)
  udp:send(string.format(observer.index .. " trigger " .. 1))
end

function Bird:display()

  -- Set color for internal state.
  local totalFear = self.fear * self.fearFactor
  local totalDesire = self.desire * self.desireFactor
  local totalHunger = self.hunger * self.hungerFactor
  local greatest = math.max(totalFear, totalDesire, totalHunger)

  if greatest == totalFear then
    love.graphics.setColor(1, self.desire / 100, self.hunger / 100, 1)
    love.graphics.circle("line", self.body:getX(), self.body:getY(), self.sight, 5)
  elseif greatest == totalDesire then
    love.graphics.setColor(self.fear / 100, 1, self.hunger / 100, 1)
    love.graphics.circle("line", self.body:getX(), self.body:getY(), self.sight, 8)
  elseif greatest == totalHunger then
    love.graphics.setColor(self.fear / 100, self.desire / 100, 1, 1)
    love.graphics.circle("line", self.body:getX(), self.body:getY(), self.sight)
  end

  if self.gender == 1 then
    love.graphics.circle("fill", self.body:getX(), self.body:getY(), self.shape:getRadius(), 4)
  elseif self.gender == 0 then
    love.graphics.circle("fill", self.body:getX(), self.body:getY(), self.shape:getRadius(), 3)
  end

  -- Display the numbers for the internal states.
--   love.graphics.print("F: " .. self.fear, self.body:getX(), self.body:getY() - 20)
--   love.graphics.print("H: " .. self.hunger, self.body:getX(), self.body:getY() - 40)
--   love.graphics.print("D: " .. self.desire, self.body:getX(), self.body:getY() - 60)
end
