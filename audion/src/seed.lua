Seed = {
  body = 0,
  fixture = 0,
  shape = 0,
  age = math.random(0, 10)
}

function Seed:new (o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end

function Seed:init()
  self.body = love.physics.newBody(ecosystem,
    math.random(WIDTH),
    math.random(HEIGHT),
    "dynamic")
  self.shape = love.physics.newCircleShape(3)
  self.fixture = love.physics.newFixture(self.body, self.shape, 1)
end

function Seed:update()
  self.age = self.age + love.timer.getDelta()
end

function Seed:display()
  love.graphics.setColor(1, 1, 1, 0.9)
  love.graphics.circle("fill", self.body:getX(), self.body:getY(), self.shape:getRadius())
end
