-- Functions for creating the interface and interacting with interface elements.
local index = 1
local contextX = nil
local contextY = nil
local contextType = nil
local currentMax = 50
local currentMin = 5

interfaceStyle = {
  font = love.graphics.newFont(love.window.toPixels(19)),
  tooltipFont = love.graphics.newFont(love.window.toPixels(18)),
  showBorder = true,
  bgColor = {0.25, 0.25, 0.25},
  radius = 5
}

contextPanelStyle = {
  font = loveFont,
  fontSize = 120,
  bgColor = {0.25, 0.25, 0.25},
  radius = 5
}

function loadInterface()

  gooi.setStyle(interfaceStyle)
  gooi.desktopMode()

  panelGrid = gooi.newPanel({x = 0, y = 0, w = 86, h = 800, layout = "grid 10x1"})

  feederButton = gooi.newButton({w = 70, h =70, text = ""})
    :onRelease(function() addAgent("F") end)
    :setBGImage(feederImage)
    :setTooltip("Add Feeder Object")

  watcherButton = gooi.newButton({size = 70, text = ""})
    :onRelease(function() addAgent("W") end)
    :setBGImage(watcherImage)
    :setTooltip("Add Watcher Object")

  hunterButton = gooi.newButton({size = 70, text = ""})
    :onRelease(function() addAgent("H") end)
    :setBGImage(hunterImage)
    :setTooltip("Add Hunter Object")

  virusButton = gooi.newButton({size = 70, text = ""})
    :onRelease(function() addAgent("V") end)
    :setBGImage(virusImage)
    :setTooltip("Add Plague Object")

  minKnob = gooi.newKnob({value = (minBirds / 25), size = 70})
    :setTooltip("Minimum Bird Count: " .. tostring(minBirds))
    :onRelease(function()
      currentMin = math.floor((minKnob:getValue() * 25))
      minKnob:setTooltip("Minimum Bird Count: " .. tostring(currentMin))
      minBirds = currentMin
    end)

  maxKnob = gooi.newKnob({value = (maxBirds / 100), size = 70})
    :setTooltip("Maximum Bird Count: " .. tostring(maxBirds))
    :onRelease(function()
      currentMax = math.floor((maxKnob:getValue() * 100))
      maxKnob:setTooltip("Maximum Bird Count: " .. tostring(currentMax))
      maxBirds = currentMax
    end)

  volumeKnob = gooi.newKnob({value = 0.2, size = 70, text = "test"})
    :setTooltip("Master Volume: " .. currentVolume)
    :onRelease(function()
      currentVolume = (volumeKnob:getValue() * 5)
      udp:send("masterVolume " .. currentVolume)
      volumeKnob:setTooltip("Master Volume: " .. currentVolume)
    end)

  dspSwitch = gooi.newCheck({value = dspON, w = 30, h = 30})
    :setTooltip("Toggle DSP")
    :onRelease(function()
      dspON = dspSwitch.checked
      if dspON == true then
        udp:send("masterDSP " .. 1)
      elseif dspON == false then
        udp:send("masterDSP " .. 0)
      end
    end)

  panelGrid:add(feederButton, watcherButton, hunterButton, virusButton,
    minKnob, maxKnob, volumeKnob, dspSwitch)

end

function loadContextMenu()

  gooi.desktopMode()
  gooi.setStyle(contextPanelStyle)

  local radius = 0.2
  local position = 0.2
  local volume = 0.2
  local seeds = 0.2
  local angle = 0.0

  if isSelected ~= nil then
    radius = isSelected.radius / 1000
    position = isSelected.startPoint
    volume = isSelected.volume / 5
    seeds = isSelected.seeds / 20
    angle = isSelected.angle / (math.pi * 2)
  end

  contextPanel = gooi.newPanel({w = 300, h = 520, layout = "grid 15x1"})
    :setGroup("context")
    :setOpaque(true)
    :setStyle(contextPanelStyle)

  panelLabel = gooi.newLabel({text = "Observer Properties", w = 300, h = 30})
    :setOpaque(true)
    :bg({0.3, 0.3, 0.5})
    :setGroup("context")

  volumeLabel = gooi.newLabel({text = "Object Volume", w = 200, h = 20})
    :setGroup("context")

  volumeSlider = gooi.newSlider({value = volume})
    :setGroup("context")
    :bg({0.3, 0.3, 0.3})
    :onRelease(function() updateData() end )

  radiusLabel = gooi.newLabel({text = "Object Radius", w = 200, h = 20})
    :setGroup("context")

  radiusSlider = gooi.newSlider({value = radius, w = 200, h = 20})
    :setGroup("context")
    :bg({0.3, 0.3, 0.3})
    :onRelease(function() updateData() end)

  fileLabel = gooi.newLabel({text = "Select File", w = 200, h = 20})
    :setGroup("context")

  filePicker = gooi.newSpinner({min = 1, max = table.getn(fileNames), value = 1,
    w = 200, h = 20})
    :setGroup("context")
    :bg({0.3, 0.3, 0.3})

  confirmFile = gooi.newButton({w = 200, h = 20, text = "Load File"})
    :setGroup("context")
    :bg({0.4, 0.3, 0.3})
    :onRelease(function() updateData() end)

  positionLabel = gooi.newLabel({text = "Start Position", w = 200, h = 20})
    :setGroup("context")

  positionSlider = gooi.newSlider({value = position, w = 200, h = 20})
    :setGroup("context")
    :bg({0.3, 0.3, 0.3})
    :onRelease(function() updateData() end)

  seedsLabel = gooi.newLabel({text = "Number of Seeds", w = 200, h = 20})
    :setGroup("context")

  seedsSlider = gooi.newSlider({value = seeds, w = 200, h = 20})
    :setGroup("context")
    :bg({0.3, 0.3, 0.3})
    :onRelease(function() updateData() end)

  angleLabel = gooi.newLabel({text = "Angle", w = 200, h = 20})
    :setGroup("context")

  angleSlider = gooi.newSlider({value = angle, w = 200, h = 20})
    :setGroup("context")
    :bg({0.3, 0.3, 0.3})
    :onRelease(function() updateData() end)

  deleteButton = gooi.newButton({text = "Delete Observer", w = 200, h = 20})
    :setGroup("context")
    :onRelease(function() deleteAgent() end)
    :bg({0.4, 0.3, 0.3})

  closeButton = gooi.newButton({text = "Close", w = 200, h = 20})
    :setGroup("context")
    :onRelease(function() popupOpen = false end)
    :bg({0.5, 0.3, 0.3})

  contextPanel:add(volumeLabel, volumeSlider, radiusLabel, radiusSlider, fileLabel,
    filePicker, confirmFile, positionLabel, positionSlider, seedsLabel, seedsSlider,
    angleLabel, angleSlider, deleteButton, closeButton)
end

function addAgent(type)
  newAgent = Observer:new()
  newAgent:init()
  newAgent.type = type
  newAgent.body = love.physics.newBody(ecosystem,
    WIDTH / 2,
    HEIGHT / 2,
    "static")
  newAgent.fixture = love.physics.newFixture(newAgent.body, newAgent.shape, 1)
  table.insert(observers, newAgent)
  newAgent.index = table.getn(observers) - 1
  newAgent.fileSent = false 
end

function closeWindow(panelObject)
  popupOpen = false
  isSelected = nil
  seedsLabel:setVisible(true)
  seedsSlider:setVisible(true)
  angleLabel:setVisible(true)
  angleSlider:setVisible(true)
end

function updateInterface()
    local x = mouseBody:getX()
    local y = mouseBody:getY()
    local xOffset = 6
    local yOffset = 37

    minKnob:setTooltip("Minimum Bird Count: " .. tostring(minBirds))
    maxKnob:setTooltip("Maximum Bird Count: " .. tostring(maxBirds))

    -- Draw context menu for the feeder type.
    if isSelected.type == "F" then
      contextPanel:setBounds(x, y, 300, 600)
      panelLabel:setBounds(x, y - 10, 300, 30)
      volumeLabel:setBounds(x + xOffset, y + (1 * yOffset))
      volumeSlider:setBounds(x + xOffset, y + (2 * yOffset))
        :setValue(isSelected.volume / 5)
      radiusLabel:setBounds(x + xOffset, y + (3 * yOffset))
      radiusSlider:setBounds(x + xOffset, y + (4 * yOffset))
        :setValue(isSelected.radius / 1000)
      fileLabel:setBounds(x + xOffset, y + (5 * yOffset))
      filePicker:setBounds(x + xOffset, y + (6 * yOffset))
      confirmFile:setBounds(x + xOffset, y + (7 * yOffset))
      positionLabel:setBounds(x + xOffset, y + (8 * yOffset))
      positionSlider:setBounds(x + xOffset, y + (9 * yOffset))
        :setValue(isSelected.startPoint)
      seedsLabel:setBounds(x + xOffset, y + (10 * yOffset)):setVisible(true)
      seedsSlider:setBounds(x + xOffset, y + (11 * yOffset))
        :setValue(isSelected.seeds / 20):setVisible(true)
      angleLabel:setBounds(x + xOffset, y + (12 * yOffset)):setVisible(true)
      angleSlider:setBounds(x + xOffset, y + (13 * yOffset))
        :setValue(isSelected.angle / (math.pi * 2)):setVisible(true)
      deleteButton:setBounds(x + xOffset, y + (14 * yOffset))
      closeButton:setBounds(x + xOffset, y + (15 * yOffset))

    -- Draw context menu for the other object types.
    else
      contextPanel:setBounds(x, y, 300, 450)
      panelLabel:setBounds(x, y - 10, 300, 30)
      volumeLabel:setBounds(x + xOffset, y + (1 * yOffset))
      volumeSlider:setBounds(x + xOffset, y + (2 * yOffset))
        :setValue(isSelected.volume / 5)
      radiusLabel:setBounds(x + xOffset, y + (3 * yOffset))
      radiusSlider:setBounds(x + xOffset, y + (4 * yOffset))
        :setValue(isSelected.radius / 1000)
      fileLabel:setBounds(x + xOffset, y + (5 * yOffset))
      filePicker:setBounds(x + xOffset, y + (6 * yOffset))
      confirmFile:setBounds(x + xOffset, y + (7 * yOffset))
      positionLabel:setBounds(x + xOffset, y + (8 * yOffset))
      positionSlider:setBounds(x + xOffset, y + (9 * yOffset))
        :setValue(isSelected.startPoint)
      deleteButton:setBounds(x + xOffset, y + (10 * yOffset))
      closeButton:setBounds(x + xOffset, y + (11 * yOffset))
      seedsLabel:setVisible(false)
      seedsSlider:setVisible(false)
      angleLabel:setVisible(false)
      angleSlider:setVisible(false)
    end
end

function deleteAgent()
  for i=table.getn(observers), 1, -1 do
    if isSelected == observers[i] then
      table.remove(observers, i)
      isSelected = nil
      popupOpen = false
    end
  end
end

function updateData()

    local newFile = fileNames[filePicker:getValue()]
    newFile = loadedFiles[newFile]
    if newFile ~= isSelected.file then
      isSelected.file = newFile
      isSelected.fileSent = false
    end

    -- Check volume
    local newVolume = volumeSlider:getValue()
    if newVolume ~= isSelected.volume then
      isSelected.volume = newVolume
      isSelected.volumeSent = false
    end

    -- Check file position
    local newPosition = positionSlider:getValue()
    if newPosition ~= isSelected.startPoint then
      isSelected.startPoint = newPosition
      isSelected.startPointSent = false
      isSelected.position = isSelected.startPoint
    end

    isSelected.radius = (radiusSlider:getValue() * 1000)
    isSelected.angle = angleSlider:getValue() * (2 * math.pi)
    isSelected.seeds = math.floor(seedsSlider:getValue() * 20)
    isSelected.volume = volumeSlider:getValue() * 7.5

end
