Observer = {
  body = 0,
  fixture = 0,
  shape = 0,
  type = 0,
  period = 60,
  delta = 0,
  radius = 200,
  angle = 0,
  watching = {},
  seeds = 5,
  file = "",
  fileSent = true,
  startPoint = 0,
  startPointSent = true,
  volume = 0.5,
  volumeSent = true,
  index = nil
}

function Observer:new (o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end

function Observer:init()
  self.body = love.physics.newBody(ecosystem,
    love.mouse.getX(),
    love.mouse.getY(),
    "static")
  self.shape = love.physics.newCircleShape(20)
  self.fixture = love.physics.newFixture(self.body, self.shape, 1)
  if self.type == "F" then
    self.angle = math.rad(math.random(0, 360))
  end
  self.file = loadedFiles[1]
  self.fileSent = false
end

function Observer:update()

  self.delta = self.delta + love.timer.getDelta()
  self.watching = {}

  -- Reset starting point for sample if it's greater than 1.
  if self.startPoint > 1 then
    self.startPoint = 0
  end

  for i=table.getn(birds), 1, -1 do
    local d = love.physics.getDistance(self.fixture, birds[i].fixture)
    if d < self.radius then
      table.insert(self.watching, birds[i])
    end
  end

  for i=1, table.getn(self.watching) do
    local theBird = self.watching[i]

    -- Procedure for the Virus type.
    if self.type == "V" then
      if self.delta >= 60 / self.period then
        local deathStrike = math.random()
        if deathStrike > 0.85 then
          theBird.age = 100
        end
      end
      self.delta = 0
    end

    -- Procedure for the Watcher type.
    if self.type == "W" then
      theBird.desire = theBird.desire + 2
      if theBird.desire > 100 then
        theBird.desire = 100
      end
    end

    -- Procedure for Hunter type.
    if self.type == "H" then
      theBird.fear = theBird.fear + 1.5
      if theBird.fear > 100 then
        theBird.fear = 100
      end
    end

  end

  -- Procedure for Feeder type.
  if self.type == "F" then
    if self.delta >= 60 / self.period then
      for i=1, self.seeds do
        local seedAngle = math.rad(math.random(-60, 60)) + self.angle
        droppedSeed = Seed:new()
        droppedSeed:init()
        local factor = math.random(self.radius / 4, self.radius)
        local x = self.body:getX() + (math.cos(seedAngle) * factor)
        local y = self.body:getY() + (math.sin(seedAngle) * factor)
        droppedSeed.body:setX(x)
        droppedSeed.body:setY(y)
        table.insert(seeds, droppedSeed)
      end
      self.delta = 0
    end
  end
end


function Observer:display()

  -- Draw the observers to the screen.
  local typeArray = {["F"] = feederImage, ["W"] = watcherImage,
    ["H"] = hunterImage, ["V"] = virusImage }

  local displayImage = typeArray[self.type]
  love.graphics.setColor(1, 1, 1, 0.85)
  love.graphics.draw(displayImage, self.body:getX(), self.body:getY(), self.angle, 0.3, 0.3,
    displayImage:getHeight() / 2, displayImage:getWidth() / 2)

  -- Draw their circle of influence.
  love.graphics.setColor(1, 1, 1, 0.05)
  love.graphics.circle("fill", self.body:getX(), self.body:getY(), self.radius)
end

function Observer:sendData(indexValue)

  -- Check for changes in the array of observers.
  if (indexValue - 1) ~= self.index then
    self.index = (indexValue - 1)
    self.fileSent = false
    self.volumeSent = false
    self.startPointSent = false
  end

  -- Send file name, volume, and file position to pd.
  if self.fileSent == false then
    love.timer.sleep(0.1)
    udp:send(self.index ..  " fileName " .. self.file)
    love.timer.sleep(0.05)
    self.fileSent = true
  end
  if self.volumeSent == false then
    udp:send(self.index .. " sampleVolume " .. self.volume)
    self.volumeSent = true
  end
  if self.startPointSent == false then
    udp:send(self.index .. " startPoint " .. self.startPoint)
    self.startPointSent = true
  end
end
