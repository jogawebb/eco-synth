-- Helper functions for ecosynth's main.lua file.

-- Load image assets.
function loadImages()

  -- Load image assets.
  feederImage = love.graphics.newImage("src/assets/images/feeder.png")
  watcherImage = love.graphics.newImage("src/assets/images/watcher.png")
  hunterImage = love.graphics.newImage("src/assets/images/hunter.png")
  virusImage = love.graphics.newImage("src/assets/images/virus.png")

end

function loadSamples()
  local files = love.filesystem.getDirectoryItems("src/assets/sounds/")
  for i=1, table.getn(files) do
    local fileAlias = files[i]
    loadedFiles[fileAlias] = "../src/assets/sounds/" .. fileAlias
    table.insert(fileNames, fileAlias)
  end
end

function drawMouse()
  love.graphics.setColor(1, 1, 1, 0.7)
  love.graphics.circle("fill", mouseBody:getX(), mouseBody:getY(), 4)
end

function masterAttributes()
  local hungerSum = 0
  local fearSum = 0
  local desireSum = 0
  local birdTotal = table.getn(birds)
  for i=birdTotal, 1, -1 do
    hungerSum = hungerSum + birds[i].hunger
    desireSum = desireSum + birds[i].desire
    fearSum = fearSum + birds[i].fear
  end
  masterFear = (fearSum / birdTotal)
  udp:send("masterDesire " .. (desireSum / birdTotal))
  udp:send("masterHunger " .. (hungerSum/birdTotal))
end
